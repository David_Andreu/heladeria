package HeladeriaMVC.base;

import java.time.LocalDate;

public class Granizado extends Producto {
    private String tamano;

    public Granizado(){super();}

    public Granizado(int cantidad, String sabor, LocalDate fechaCompra, String tamano){
        super(cantidad, sabor, fechaCompra);
        this.tamano=tamano;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    @Override
    public String toString() {
        return "Granizado{" +
                "cantidad= "+getCantidad()+
                "sabor= "+getSabor()+
                "tamano='" + tamano + '\'' +
                '}';
    }
}
