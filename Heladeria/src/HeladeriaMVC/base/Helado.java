package HeladeriaMVC.base;

import java.time.LocalDate;

public class Helado extends Producto {

    private int numeroBolas;

    public Helado(){super();}

    public Helado(int cantidad, String sabor, LocalDate fechaCompra, int numeroBolas){
        super(cantidad, sabor, fechaCompra);
        this.numeroBolas=numeroBolas;
    }

    public int getNumeroBolas() {
        return numeroBolas;
    }

    public void setNumeroBolas(int numeroBolas) {
        this.numeroBolas = numeroBolas;
    }

    @Override
    public String toString() {
        return "Helado{" +
                "cantidad= "+getCantidad()+
                "sabor= "+getSabor()
                +"numeroBolas=" + numeroBolas +
                '}';
    }
}
