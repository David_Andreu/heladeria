package HeladeriaMVC.base;

import java.time.LocalDate;

public class Gofre extends Producto {
    private String acompanante;

    public Gofre(){super();}

    public Gofre(int cantidad, String sabor, LocalDate fechaCompra, String acompanante){
        super(cantidad, sabor, fechaCompra);
        this.acompanante=acompanante;
    }

    public String getAcompanante() {
        return acompanante;
    }

    public void setAcompanante(String acompanante) {
        this.acompanante = acompanante;
    }

    @Override
    public String toString() {
        return "Gofre{" +
                "cantidad= "+getCantidad()+
                "sabor= "+getSabor()+
                "acompanante='" + acompanante + '\'' +
                '}';
    }
}
