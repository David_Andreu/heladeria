package HeladeriaMVC.base;

import java.time.LocalDate;

public abstract class Producto {
    private int cantidad;
    private String sabor;
    private LocalDate fechaCompra;

    public Producto(){

    }

    public Producto(int cantidad, String sabor, LocalDate fechaCompra){
        this.cantidad=cantidad;
        this.sabor=sabor;
        this.fechaCompra=fechaCompra;

    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getSabor() {
        return sabor;
    }

    public void setSabor(String sabor) {
        this.sabor = sabor;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
}
