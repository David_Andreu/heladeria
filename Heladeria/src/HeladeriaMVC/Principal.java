package HeladeriaMVC;

import HeladeriaMVC.GUI.HeladeriaControlador;
import HeladeriaMVC.GUI.HeladeriaModelo;
import HeladeriaMVC.GUI.Ventana;

public class Principal {
    public static void main(String[] args) {
        Ventana vista=new Ventana();
        HeladeriaModelo modelo=new HeladeriaModelo();
        HeladeriaControlador controlador=new HeladeriaControlador(vista,modelo);
    }
}
