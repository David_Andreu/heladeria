package HeladeriaMVC.GUI;

import HeladeriaMVC.base.Producto;

import javax.swing.*;

public class Ventana {
    private JPanel panel1;
    private JRadioButton heladoRadioButton;
    private JRadioButton gofreRadioButton;
    private JRadioButton granizadoRadioButton;
    private JTextField saborTextField;
    private JTextField textField1;
    private JTextField textField2;
    private JButton nuevoButton;
    private JButton exportarButton;
    private JButton importarButton;
    private JList list1;

    public JFrame frame;
    public DefaultListModel<Producto> dlmProducto;

    public Ventana(){
        frame=new JFrame("HeladeriaMVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        initComponents();
    }

    private void initComponents() {
        dlmProducto=new DefaultListModel<Producto>();
        list1.setModel(dlmProducto);
    }
}
