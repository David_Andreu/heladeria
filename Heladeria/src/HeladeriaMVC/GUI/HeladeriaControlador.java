package HeladeriaMVC.GUI;

import HeladeriaMVC.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class HeladeriaControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private HeladeriaModelo modelo;
    private File ultimaRutaExportada;

    public HeladeriaControlador(Ventana vista, HeladeriaModelo modelo)  {
        this.vista=vista;
        this.modelo=modelo;
        try {
            cargarDatosConfiguracion();
        }catch (IOException e){
            System.out.println("No exists el fichero de configuracion "+e.getMessage());
        }



    }

    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion=new Properties();
        configuracion.load(new FileReader("heladeria.conf"));
        ultimaRutaExportada=new File(configuracion.getProperty("ultimaRutaExportada"));

    }

    private void actializarDatosConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada=ultimaRutaExportada;
    }

    private void guardarDatosConfiguracion() throws IOException {
        Properties configuracion=new Properties();
        configuracion.setProperty("ultimaRutaExportada",ultimaRutaExportada.getAbsolutePath());

        configuracion.store(new PrintWriter("heladeria.conf"),"Datos configuracion heladeria");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {


    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
